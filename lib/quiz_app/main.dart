import 'package:flutter/material.dart';

import './quiz.dart';
import './result.dart';

void main() => runApp(MyApp());

class MyApp extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return _MyAppState();
  }
}

class _MyAppState extends State<MyApp> {
  final _questions = const [
    {
      'questionText': 'Siapa presiden pertama RI?',
      'answers': [
        {'text': 'Sukarno', 'score': 1},
        {'text': 'Suharto', 'score': 0},
        {'text': 'Megawati', 'score': 0},
        {'text': 'Habibie', 'score': 0}
      ],
    },
    {
      'questionText': 'Agama Konghucu disahkan ketika kepemimpinan presiden?',
      'answers': [
        {'text': 'Suharto', 'score': 0},
        {'text': 'Megawati', 'score': 0},
        {'text': 'Habibie', 'score': 0},
        {'text': 'Gus Dur', 'score': 1},
        {'text': 'SBY', 'score': 0},
      ],
    },
    {
      'questionText': 'Ormas Islam terbesar di Indonesia adalah?',
      'answers': [
        {'text': 'FPI', 'score': 0},
        {'text': 'Muhammadiyah', 'score': 0},
        {'text': 'Nahdlatul Ulama', 'score': 1},
        {'text': 'Nahdlatul Wathon', 'score': 0}
      ],
    },
  ];

  var _questionIndex = 0;
  var _totalScore = 0;

  void _resetQuiz() {
    setState(() {
      _questionIndex = 0;
      _totalScore = 0;
    });
  }

  void _answerQuestion(int score) {
    _totalScore += score;

    setState(() {
      _questionIndex = _questionIndex + 1;
    });

    print(_questionIndex);
    if (_questionIndex < _questions.length) {
      print('We have more questions!');
    } else {
      print('No more questions!');
    }
  }

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: Scaffold(
        appBar: AppBar(
          title: Text('My First App'),
        ),
        body: _questionIndex < _questions.length
            ? Quiz(
                answerQuestion: _answerQuestion,
                questionIndex: _questionIndex,
                questions: _questions,
              )
            : Result(_totalScore, _resetQuiz),
      ),
    );
  }
}
